const express = require('express');
const app = express();
const http = require("http");
const { Server } = require('socket.io');
const cors = require("cors");

app.use(cors());

const server = http.createServer(app);

const io = new Server(server, {
    cors: {
        origin: ["https://643c4c3878bd4f0e34f691ce--stalwart-kitten-e261ab.netlify.app"],
        methods: ["GET", "POST"],
    },
});
  // initialize timer variables
var moleTarget = null;
let timeRemaining = 20;
let timerStarted = false;
let intervalId = null;
let score = 0;


io.on("connection", (socket) => {
    console.log(`User connected: ${socket.id}`)
    socket.join("room1");
    const users = Array.from(io.sockets.adapter.rooms.get("room1"));
    console.log(`User with id ${socket.id} joined. Number of clients in room1: ${users.length}`);
    console.log(`The current members are ${users}`);

    const serverTime = Date.now();
    socket.emit("server_time", serverTime);

   


  const startTimer = () => {
    if (intervalId) {
      // timer already started
      return;
    }
    console.log("Starting timer");
    timerStarted = true;
    intervalId = setInterval(() => {
      if (timeRemaining > 0) {
        timeRemaining--;
        io.to("room1").emit("timer_tick", timeRemaining);
      } else {
        // stop timer and notify clients
        clearInterval(intervalId);
        intervalId = null;
        io.to("room1").emit("timer_end");
      }
    }, 1000);
  };

  const stopTimer = () => {
    console.log("Stopping timer");
    clearInterval(intervalId);
    moleTarget = null;
    intervalId = null;
    timeRemaining = 20;
    timerStarted = false;
  };

  socket.on("start_timer", () => {
    score = 0;
    stopTimer();
    startTimer();
    generateMole();
  });

  socket.on("stop_timer", () => {
    stopTimer();
  });
    


  socket.on("mole_clicked", () => {
    console.log(`Mole clicked by user: ${socket.id}`);
    if (socket.id === moleTarget) {
      score += 1;
      io.to("room1").emit("score_updated", score);
      generateMole();
    }
  });
  

  function generateMole (){
    moleTarget = null;
        const users = Array.from(io.sockets.adapter.rooms.get("room1"));
        if (users.length === 1 && timerStarted) {
            moleTarget = users[0];
        } else if (users.length > 1) {
            let randomIndex = Math.floor(Math.random() * users.length);
            while (users[randomIndex] === moleTarget) {
                randomIndex = Math.floor(Math.random() * users.length);
            }
            moleTarget = users[randomIndex];
        }
        console.log(`Mole target is: ${moleTarget}`);
        io.to("room1").emit("mole_generated", moleTarget);
    
}

socket.on("generate_mole", () => {
  generateMole();
});


    socket.on("disconnect", () => {
      console.log(`User disconnected: ${socket.id}`);
      if (socket.id === moleTarget) {
        moleTarget = null;
      }
      if (users.length === 1 && timerStarted) {
        stopTimer();
      }
    });
  });

  
  

server.listen(3001, ()=> {
    console.log("Server is listening on port 3001")
})

server.on("error", (error) => {
    console.error("Server error:", error);
  });
  